import Menu from '../Menu/Menu';
import SearchRow from '../SearchRow';

import {getToDoList} from '../../service/getToDoList';

export default function RightPanel() {
    return (
        <div className='right-panel'>
            <SearchRow/>
            <div className='right-panel__body'>
                <Menu menu={getToDoList()}/>
            </div>
            <div className='container'>
                <div className='right-panel__footer row row_end'>
                    <button className='button'>
                        Добавить задачу
                    </button>
                </div>
            </div>
        </div>
    );
}
