import {useState} from 'react';
import clsx from 'clsx';

export default function MenuItem({item, level}) {
    const [open, setOpen] = useState(false);

    return (
        <div className={clsx('menu__node', {menu__node_opened: open})}>
            <button
                onClick={() => setOpen(!open)}
                className='menu__button text text_overflow'
                title={item.title}
                style={{paddingLeft: level * 16}}
            >
                {item.children &&
                    <i
                        className={clsx('menu__arrow icon icon_chevron-right', {'icon_chevron-down': open})}/>
                }
                <span className='menu__label'>
                    {item.title}
                </span>
            </button>
            <div className={clsx('menu__body', {menu__body_show: open})}>
                {item.children?.map(
                    item =>
                        <MenuItem level={level + 1} key={item.title} item={item}/>
                )}
            </div>
        </div>
    );
}
