import MenuItem from './MenuItem';

export default function Menu({menu, level = 1}) {
    return (
        <div className='menu'>
            {menu.map(item => <MenuItem key={item.title} item={item} level={level}/>)}
        </div>
    );
}
