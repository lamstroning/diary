export default function Option({label, value, setValue}) {
    return (
        <button
            className='option'
            onClick={() => setValue(value)}
        >
            {label}
        </button>
    );
}
