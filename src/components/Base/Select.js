import Option from './Option';
import {useState} from 'react';
import clsx from 'clsx';

export default function Select({label, options, placeholder = 'Выберете значение'}) {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState('');

    function onChange(value) {
        setValue(value);
        setOpen(false);
    }

    if (!options)
        return null;

    return (
        <>
            <div
                className={clsx('background-close-trigger', {'background-close-trigger_active': open})}
                onClick={() => setOpen(false)}
            />
            <div className='form-control'>
                <div className='form-control__label'>
                    {label}
                </div>
                <div className={clsx('form-control__field select', {select_active: open})}>
                    <button
                        className='select__value'
                        onClick={() => setOpen(!open)}
                    >
                        {value ||
                            <span className='select__placeholder'>
                                {placeholder}
                            </span>
                        }
                        <div className='select__icon'>
                            <i className='icon icon_chevron-down'/>
                        </div>
                    </button>
                    <div className='select__option-list'>
                        {options.map(({value, label}) =>
                            <Option
                                key={value}
                                setValue={onChange}
                                value={value}
                                label={label}
                            />
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}
