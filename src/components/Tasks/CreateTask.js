import Input from '../Base/Input';
import Select from '../Base/Select';
const options = [
    {
        value: 'IT',
        label: 'IT'
    },
    {
        value: 'Medic',
        label: 'Medic'
    },
    {
        value: 'Flat',
        label: 'Flat'
    }
];

export default function CreateTask() {
    return (
        <>
            <div className='row'>
                <div className='section section_top-clear'>
                    <div className='col'>
                        <Select
                            label='Категория'
                            options={options}
                            placeholder='Выберете категорию'
                        />
                    </div>
                </div>
            </div>
            <div className='row'>
                <div className='section'>
                    <div className='col'>
                        <Input
                            label='Категория'
                        />
                    </div>
                </div>
            </div>
        </>
    );
}
