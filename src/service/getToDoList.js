const menu = [
    {
        title: 'IT',
        children: [
            {
                title: 'Работа'
            },
            {
                title: 'Фриланс',
                children: [
                    {
                        title: 'Фриланс'
                    },
                    {
                        title: 'Фриланс'
                    }
                ]
            }
        ]
    },
    {
        title: 'Medic',
        children: [
            {
                title: 'Физиолог'
            },
            {
                title: 'Терапевт'
            }
        ]
    },
    {
        title: 'Flat'
    }
];


export function saveToDoList() {
    localStorage.setItem('ToDoList', JSON.stringify(getToDoList()));
}

export function getToDoList() {
    // return JSON.parse(localStorage.getItem('ToDoList') || );
    return menu;
}
